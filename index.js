const https = require('https');
const AWS = require("aws-sdk");

console.log('Loading function');

exports.handler = function (event, context, callback) {
    //Get the message from SNS
    const message = event.Records[0].Sns.Message;//{"id":1,"description":"Test Payload"}
    console.log('From SNS:', message);

    //Parse the message to JSON
    let data = JSON.parse(message);
    console.log('JSON:', data);

    //Append the id from the message
    const url = `https://api.abcotvs.com/v3/kabc/item/${data.id}?key=test`;
    console.log('Consuming URL:', url);

    //Create the client to consume the URL
    https.get(url, (res) => {

        let body = '';
        let parsed = {};
        
        // incrementally capture the incoming response body
        res.on('data', function (d) {
            body += d;
        });

        // do whatever we want with the response once it's done
        res.on('end', function () {
            try {
                parsed = JSON.parse(body);
            } catch (err) {
                console.error('Unable to parse response as JSON', err);
                return callback(Error(err));
            }

            // pass the relevant data back to the callback
            const responseStr = JSON.stringify(parsed);
            console.log("The Response: ", responseStr);
            
            console.log("Sending to the SNS2...");
            sendToSNS(responseStr, context);
            callback(null, res.statusCode);

        });

    }).on('error', (e) => {
        callback(Error(e))
    })
}

function sendToSNS(message, context){
    
    const sns = new AWS.SNS();
    const params = {
        Message: message, 
        Subject: "Test SNS From Lambda",
        TopicArn: "arn:aws:sns:us-east-2:593381551022:Test2"
    };
    console.log("Sending data: ", JSON.stringify(params));

    sns.publish(params, context.done);
}